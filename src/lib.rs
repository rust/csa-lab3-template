pub fn add_one(v: i32) -> i32 {
    v + 1
}

// Юнит тесты (для запуска - cargo run)

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(add_one(1), 2);
    }
}
