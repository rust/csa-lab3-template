use std::error::Error;

use csa_lab3::add_one;

fn main() -> Result<(), Box<dyn Error>> {
    let mut line = String::new();
    std::io::stdin().read_line(&mut line)?;

    let number = line.trim().parse::<i32>()?;
    let result = add_one(number);

    println!("Hello, world!");
    println!("{number} + 1 = {result}");

    Ok(())
}
