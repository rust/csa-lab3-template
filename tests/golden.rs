//! Простейший фреймворк для golden-тестирования
//!
//! Golden тесты лежат в папке tests/golden
//! При тестировании запускается бинарнику передается указанный в файле теста stdin, а затем сравнивается
//! вывод stdout и stderr. При желании можно дополнить код передачей/сравнением файлов, аргументами командной строки и т.д.
//!
//! Для запуска - cargo test

use std::error::Error;
use std::io::Write;
use std::process::{Command, Stdio};

use serde::Deserialize;

#[derive(Deserialize)]
struct GoldenTest {
    in_stdin: String,
    out_stdout: String,
    out_stderr: String,
}

#[test]
fn golden_tests() -> Result<(), Box<dyn Error>> {
    let root = concat!(env!("CARGO_MANIFEST_DIR"), "/tests/golden");
    let mut all_passed = true;

    for entry in std::fs::read_dir(root)? {
        let path = entry?.path();
        if path.extension().and_then(|s| s.to_str()) != Some("yaml") {
            continue;
        }

        let test_name = path.file_stem().and_then(|s| s.to_str()).unwrap();

        let yaml = match std::fs::read_to_string(&path) {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Failed to open {}, skipping: {e:?}", path.display());
                continue;
            }
        };

        // Парсим файл теста

        let test: GoldenTest = serde_yaml::from_str(&yaml)?;

        // Запускаем дочерний процесс

        let mut child = Command::new(env!("CARGO_BIN_EXE_csa-lab3"))
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;

        // Передаем stdin

        let mut stdin = child.stdin.take().unwrap();
        stdin.write_all(test.in_stdin.as_bytes())?;
        stdin.flush()?;
        drop(stdin);

        let output = child.wait_with_output()?;

        let got_stdout = String::from_utf8(output.stdout)?;
        let got_stderr = String::from_utf8(output.stderr)?;

        let diffs = [
            ("stdout", test.out_stdout.trim(), got_stdout.trim()),
            ("stderr", test.out_stderr.trim(), got_stderr.trim()),
            // тут можно добавить дополнительные строки для сравнения, например если ваша программа пишет в файл
        ];

        if diffs.iter().all(|(_, expected, got)| expected == got) {
            println!("PASS {}", test_name);
            continue;
        }

        all_passed = false;

        println!("FAIL {test_name}");
        println!();

        println!("stdin:");
        println!("{}", test.in_stdin);
        println!();

        for (name, expected, got) in diffs {
            println!("expected {name}:");
            if expected.is_empty() {
                println!("<empty>")
            } else {
                println!("{}", expected);
            }

            println!();

            println!("got {name}:");
            if got.is_empty() {
                println!("<empty>")
            } else {
                println!("{}", got);
            }

            println!();

            let expected = expected.lines().collect::<Vec<_>>();
            let got = got.lines().collect::<Vec<_>>();

            let diff = difflib::unified_diff(
                &expected,
                &got,
                "Expected",
                "Got",
                "1970-01-01 00:00:00",
                "1970-01-01 00:00:00",
                3,
            );

            if diff.len() > 2 {
                for line in &diff[2..] {
                    println!("{}", line);
                }

                println!();
            }
        }
    }

    assert!(all_passed, "all tests must pass");

    Ok(())
}
